from game2048.textual_2048 import *
from game2048.grid import *
from game2048.move import *
import time



#-----------INITIALIZATION-----------


size = read_size_grid()

theme = read_theme()

grid = init_game(size)

is_over = False




#-----------MAIN LOOOP-----------

while not(is_over):

    print(grid_to_string(grid,size,theme))

    wanted_move =read_player_command()
    grid_old = copy.deepcopy(grid)
    if wanted_move in ['g','d','h','b'] and move_possible(grid)[wanted_move]:
        grid_new = move_grid(grid,wanted_move)

        grid_new = grid_add_new_tile(grid_new)

        is_over = is_game_over(grid_new)

    else:
        print('move unavailable')




